FROM nginx:alpine

LABEL maintainer="randon@ayana.io"

COPY build /usr/share/nginx/html

EXPOSE 80