import React, { Component } from 'react';

import './styles.css';

class Button extends Component {
 constructor(props) {
  super(props);

  this.text = props.text || 'Text';
  this.onClicked = props.onClicked || null;
 }

 handleClick() {
  if (!this.onClicked || typeof this.onClicked !== 'function') return;
 
  this.onClicked();
 }

 render() {
  return(
   <div className='ui-button' onClick={this.handleClick.bind(this)}>
    <span>{this.text}</span>  
   </div>
  );
 }
}

export default Button;
