import React from 'react';

import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { Homepage } from './scenes';

export default store => {
 return (
  <BrowserRouter>
   <Switch>

    {/* Main Site Routes */}
    <Route path='/'>
      <Switch>
       <Route path='/' exact component={Homepage} />
      </Switch>
    </Route>

   </Switch>
  </BrowserRouter>
 );
};
