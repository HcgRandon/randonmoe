import React, { Component } from 'react';

import './styles.css';

class Socials extends Component {
 constructor(props) {
  super(props);

  this.urls = {
   youtube: 'https://www.youtube.com/user/Hcgrandon',
   twitter: 'https://twitter.com/HcgRandon',
   paypal: 'https://www.paypal.me/Devoverflow',
   github: 'https://github.com/hcgrandon',
   email: 'mailto:me@randon.moe',
  };
 }

 render() {
  return (
   <div className='socials'>
    {Object.entries(this.urls).map(([k, v]) => {
     return (
      <a href={v} style={{ backgroundImage: `url('/img/social/${k}.svg')` }} target='_blank' />
     );
    })}
   </div>
  );
 }
}

export default Socials;
