import React, { Component } from 'react';

import './styles.css';

import Socials from './components/Socials';

import Button from '../../components/UI/Button';

class Homepage extends Component {
 constructor(props) {
  super(props);
 }
 
 render() {
  return (
   <div className='hp'>
    <div className='hp__shadebox'></div>
    <div className='hp__about'>
     <div className='about__avatar'></div>
     <div className='about__username'>HcgRandon</div>
     <Socials />

     { /* <Button text='View Projects' /> */ }
    </div>
   </div>
  );
 }
}

export default Homepage;
